;;; afp-origami.el ---                               -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Thibaut Verron

;; Author: Thibaut Verron <verron@rossini>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package origami)

(defun afp-origami-find-nodes (beg end create)
  (let ((acc nil) (open t))
    (save-excursion
      (goto-char beg)
      (while (and open (< (point) end)
	(let* ((open (afp-find-next-opener end)) ;; (pos, type, string)
	       (pos (car open))
	       (type (cadr open))
	       (string (caddr open))
	       (offset (length string)))
	  (when open
	    (goto-char pos)
	    (let* ((close (afp-find-closer pos type string end))
		   (children (afp-origami-find-nodes (+ pos offset) close create)))
	      (setq acc (cons (funcall create pos close offset children)
			      acc))
	      (goto-char close)))))))
    (reverse acc)))

;; For tests we can set create to 'list

(defun afp-origami-parser (create)
  (lambda (content)
    (with-temp-buffer
      (insert content)
      (let ((LaTeX-mode-hook nil)) (latex-mode))
      (afp-origami-find-nodes (point-min) (point-max) create))))

(add-to-list
 'origami-parser-alist
 '(latex-mode . afp-origami-parser))

(provide 'afp-origami)
;;; afp-origami.el ends here
