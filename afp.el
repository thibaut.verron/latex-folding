;;; afp.el --- Folding points for latex buffers      -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Thibaut Verron

;; Author: Thibaut Verron <verron@rossini>
;; Keywords: convenience, languages, outlines

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; 

(defun afp-at-opener ()
  "Test if we are at an opener, and if so, returns its type.

The type is either '(env name desc) or '(nav name desc)."
  
  )

;; TODO: Items?

(defun afp-min-maybe (&rest args)
  (eval (cons 'min (seq-filter #'identity args))))

(defun afp-min-type (navpos envpos itempos)
  (let ((val (min navpos envpos itempos)))
    (cond
     ((= val navpos) (cons val 'nav))
     ((= val envpos) (cons val 'env))
     ((= val itempos) (cons val 'item)))))

(defun afp-find-next-nav (&optional bound)
  (save-excursion
    (outline-next-heading)
    (afp-min-maybe bound (point))))

(defun afp-find-next-begin (&optional bound)
  (save-excursion
    (or
     (and
      (re-search-forward LaTeX-begin-regexp bound 'move)
      (match-beginning 0))
     (point))))

(defun afp-find-next-item (&optional bound)
  (save-excursion
    (or
     (and
      (re-search-forward LaTeX-item-regexp bound 'move)
      (match-beginning 0))
     (point))))


(defun afp-find-next-opener (&optional bound)
  ;; Find next nav or environment opener
  ;; Output is (pos type text)
  (save-excursion
    (let* ((bound (or bound (point-max)))
	   (next-nav (afp-find-next-nav bound))
	   (next-env (afp-find-next-begin bound))
	   (next-item (afp-find-next-item bound))
	   (next-thing (afp-min-type next-nav next-env next-item))
	   (next-pos (car next-thing)))
      (unless (= next-pos bound)
	(goto-char next-pos)
	(list next-pos
	      (cdr next-thing)		
	      (buffer-substring-no-properties
	       next-pos
	       (line-end-position)))))))

(defun afp-find-env-end (&optional bound)
  ;; If called from within a comment, the environment navigation commands fail
  (save-excursion
    (let ((LaTeX-syntactic-comments nil))
      (condition-case nil
	  (progn
	    (LaTeX-find-matching-end)
	    (afp-min-maybe bound (point)))
	(error nil)))))

(defun afp-find-nav-end (&optional bound)
  ;; outline-get-next-sibling almost does the right thing, but does not identify
  ;; higher level sections
  (save-excursion
    (let ((bound (or bound (point-max)))
	  (level (outline-level))
	  (first t))
      (while (and (< (point) bound)
		  (or first
		      (< (outline-level) level)))
	(outline-next-heading)))
    (point)))
  
(defun afp-find-closer (pos type string &optional bound)
  "Find the first closer for opener OP.

If OP is the beginning of an environment, the closer is the end
of that environment, or an unexpected end of another environment,
whichever comes first.

If OP is a sectioning command, the closer is another sectioning
command of the same or higher level and not within an environment,
or an end of environment not matching a begin, whichever comes first."
  (save-excursion
    (goto-char (+ pos (length string)))
    (let ((is-nav (eq type 'nav))
	  (env-end (afp-find-env-end bound)))
      (afp-min-maybe env-end
	   (and is-nav
		(afp-find-nav-end env-end))))))

(defun afp-find-block ()
  "If point is at a block opener, find the corresponding closer.

Returns a list `\'(begin end desc)' where begin is the position
before the opener, end is the position after the closer and desc
is a suitable string to use to replace the block when folded."
  
  )

(provide 'afp)
;;; afp.el ends here
